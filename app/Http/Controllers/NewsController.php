<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use App\Category;
use App\User;
use Validator;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = array();
        $news = News::where('status', News::ENABLE)->get();
        foreach($news as $key => $value){
            $response[$key] = $value;
            $response[$key]['category_id'] = Category::getCategory($value->category_id);
            $response[$key]['user_id'] = User::getUser($value->user_id);
        }
        if(!$news->count()){
            return $this->errorResponse(self::ERROR_BAD_REQUEST, [], self::EMPTY);
        }else{
            return $this->successResponse($response, self::SUCCESSFUL);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, News $news)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'title' => 'required|min:3|max:150',
                'description' => 'required',
                'image' => 'required',
                'category_id' => 'numeric|required',
                'user_id' => 'numeric|required',
                'status' => 'required|numeric'
            ]);
            if($validator->fails()){
                return $this->errorResponse(self::ERROR_BAD_REQUEST, [], $validator->errors()->all());
            }
        $news->create($request->all());
        return $this->successResponse([], self::CREATE);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        $news->category_id = Category::getCategory($news->category_id);
        $news->user_id = User::getUser($news->user_id);
        return $this->successResponse($news, self::SUCCESSFUL);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $validator = Validator::make($request->all(),
        [
            'title' => 'min:3|max:150',
            'category_id' => 'numeric',
            'user_id' => 'numeric',
            'status' => 'numeric'
        ]);
        if($validator->fails()){
            return $this->errorResponse(self::ERROR_BAD_REQUEST, [], $validator->errors()->all());
        }
        $news->update($request->all());
        return $this->successResponse([], self::UPDATE);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();
        return $this->successResponse([], self::DELETE);
    }
}
