<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    //status code
    const ERROR_BAD_REQUEST = 400;

    //message
    const SUCCESSFUL = 'Get Data Successful';
    const EMPTY = "Empty Data";
    const CREATE = 'Create Successful';
    const UPDATE = 'Update Successful';
    const DELETE = 'Delete Successful';

    public function successResponse($data, $msg)
    {
        return response()->json(['err' => 0, 'data' => $data, 'msg' => $msg]);
    }

    public function errorResponse($err, $data, $msg)
    {
        return response()->json(['err' => $err, 'data' => $data, 'msg' => $msg]);
    }
}
