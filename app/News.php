<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends BaseModel
{
    protected $table = 'news';

    protected $fillable = [
        'title', 'description', 'image', 'category_id', 'user_id', 'status'
    ];

    public function category()
    {
        return $this->belongsTo('App\News', 'category_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
