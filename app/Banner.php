<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends BaseModel
{
    protected $table = 'banners';
}
