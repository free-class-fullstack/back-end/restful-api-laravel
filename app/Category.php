<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public function news()
    {
        return $this->hasMany('App\News', 'category_id', 'id');
    }

    public static function getCategory($id)
    {
        return Category::select('id','title', 'description')->where('id', $id)->get();
    }
}
