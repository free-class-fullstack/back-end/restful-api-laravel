<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    const ENABLE = 1;
    const DISABLE = 0;
}
