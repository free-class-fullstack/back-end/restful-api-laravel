<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logo extends BaseModel
{
    protected $table = 'logos';
}
