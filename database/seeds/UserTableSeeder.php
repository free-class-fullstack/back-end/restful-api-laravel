<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 5;
        for ($i=0; $i < 5 ; $i++) {
            DB::table('users')->insert([
                'name' => $faker->name(),
                'email' => $faker->email,
                'password' => bcrypt($faker->password()),
                'avatar' => $faker->image()
            ]);
        }
    }
}
