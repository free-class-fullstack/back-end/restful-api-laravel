<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 5;
        for($i = 0; $i < $limit; $i++){
            DB::table('banners')->insert([
                'title' => $faker->realText(100),
                'description' => $faker->realText(100),
                'image' => $faker->image(),
                'status' => $faker->boolean
            ]);
        }
    }
}
