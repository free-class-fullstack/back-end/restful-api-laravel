<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 5;
        for ($i=0; $i < 5 ; $i++) {
            DB::table('news')->insert([
                'title' => $faker->title(),
                'description' => $faker->title(),
                'image' => $faker->image(),
                'category_id' => $faker->numberBetween(1, 5),
                'user_id' => $faker->numberBetween(1, 5),
                'status' => $faker->boolean()
            ]);
        }
    }
}
