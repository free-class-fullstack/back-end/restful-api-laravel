<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LogoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 5;
        for ($i=0; $i < 5; $i++) {
            DB::table('logos')->insert([
                'image' => $faker->image(),
                'status' => $faker->boolean()
            ]);
        }
    }
}
