<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(BannerTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(LogoTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
